﻿using Newtonsoft.Json;
using System.IO;

namespace BlockChainDemo
{
    class JsonStreamer
    {

        public void ToFile(object block)
        {
            int blockName = 0;
            DirectoryInfo directory = Directory.CreateDirectory(System.Environment.CurrentDirectory + @"\Node");
            if (directory.Exists)
            {
                // ищем в корневом каталоге
                blockName += directory.GetFiles(@"*.json", SearchOption.TopDirectoryOnly).Length;
            }

            using (StreamWriter file = File.CreateText(directory + $"\\block_{blockName}.json"))
            {
                JsonSerializer serializer = new JsonSerializer();
                //serialize object directly into file stream
                serializer.Serialize(file, block);
            }
        }

    }
}
