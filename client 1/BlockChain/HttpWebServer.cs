﻿using System;
using System.Net;
using System.Text;
using System.Threading;

namespace BlockChainDemo
{
    public class HttpWebServer
    {
        private readonly HttpListener _listener = new HttpListener();
        private readonly Func<HttpListenerRequest, string> _handler;

        public HttpWebServer(Func<HttpListenerRequest, string> handler, params string[] urls)
        {
            if (urls == null || urls.Length == 0)
                throw new ArgumentException("prefixes");
            if (handler == null)
                throw new ArgumentException("method");

            foreach (string s in urls)
                _listener.Prefixes.Add(s);

            _handler = handler;
            _listener.Start();
        }

        public void Run()
        {
            ThreadPool.QueueUserWorkItem(o =>
            {
                while (_listener.IsListening)
                {
                    ThreadPool.QueueUserWorkItem(c =>
                    {
                        var ctx = c as HttpListenerContext;
                        if (ctx != null)
                        {
                            try
                            {
                                var responseStr = _handler(ctx.Request);
                                var buf = Encoding.UTF8.GetBytes(responseStr.ToString());
                                ctx.Response.ContentLength64 = buf.Length;
                                ctx.Response.Headers.Add("Access-Control-Allow-Origin", "*");
                                ctx.Response.OutputStream.Write(buf, 0, buf.Length);
                            }
                            finally
                            {
                                ctx.Response.OutputStream.Close();
                            }
                        }
                    }, _listener.GetContext());
                }
            });
        }

        public void Stop()
        {
            _listener.Stop();
            _listener.Close();
        }

        ~HttpWebServer()
        {
            Stop();
        }
    }
}
