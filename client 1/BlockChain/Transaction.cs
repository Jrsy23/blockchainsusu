﻿namespace BlockChainDemo
{
    public class Transaction
    {
        public string Message { get; set; }
        public string Recipient { get; set; }
        public string Sender { get; set; }
    }
}