﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Front.Models
{
    public class Employee
    {

        public int id { get; set; }
        public string inn_code { get; set; }
        public string passport_ser { get; set; }
        public string passport_num { get; set; }
        public string surname { get; set; }
        public string name { get; set; }
        public string pat_name { get; set; }


    }
}