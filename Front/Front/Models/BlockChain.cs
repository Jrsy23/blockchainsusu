﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Security.Cryptography;
using System.Text;

namespace Front.Models
{
     

    public class BlockChain
    {
        public List<Block> Chain { get; set; }

        public Block CreateBlock(string sender, string message, string recipient, byte [] prevBlockHash)
        {
            var newBlock = new Block
            {
                Sender = sender,
                Message = message,
                Recipient = recipient,
                PreviousBlockHash = prevBlockHash
            };
            return newBlock;
        }
        public Block CreateBlock()
        {
            var newBlock = new Block
            {
                Sender = "user1",
                Message = "block",
                Recipient = "user2",
                PreviousBlockHash = null
            };
            return newBlock;
        }

        public Block LastBlock()
        {
            return Chain.Last();
        }

        public SHA256 HashBlock(Block block)
        {
            var blockstr = SHA256.Create();
            byte[] bytes = Encoding.ASCII.GetBytes($"{block.Sender}{block.Recipient}{block.Message}");
            blockstr.ComputeHash(bytes);
            return blockstr;
        }

        BlockChain()
        {
            BlockChain chain = new BlockChain();
            Chain.Add(chain.CreateBlock());
        }
    }    
}