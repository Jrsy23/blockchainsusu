﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication1.Models
{
    public class LogInModel
    {
        public string inn_code { get; set; }
        public string password { get; set; }

    }

    public class RegInModel
    {
        public string inn_code { get; set; }
        public string name { get; set; }
        public string email { get; set; }
        public string password { get; set; }

    }


    public class AddEmployeeModel
    {
        public int id { get; set; }
        public string inn_code { get; set; }
        public string passport_ser { get; set; }
        public string passport_num { get; set; }
        public string surname { get; set; }
        public string name { get; set; }
        public string pat_name { get; set; }
    }







}